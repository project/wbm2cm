<?php

namespace Drupal\wbm2cm\Plugin\migrate\source\d7;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 webform source from database.
 *
 * @MigrateSource(
 *   id = "d7_wb2cm_node_types",
 *   core = {7},
 *   source_module = "workbench_moderation",
 *   destination_module = "content_moderation"
 * )
 */
class Wb2CmNodeTypes extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('variable', 'v');
    $query->condition('value', '%moderation%', 'LIKE');
    $query->condition('name', 'node_options_%', 'LIKE');
    $query->fields('v');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'name' => $this->t('Name'),
      'value' => $this->t('value'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['name']['type'] = 'string';
    $ids['name']['alias'] = 'v';
    return $ids;
  }

}
