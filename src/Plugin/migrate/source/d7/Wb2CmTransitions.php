<?php

namespace Drupal\wbm2cm\Plugin\migrate\source\d7;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 webform source from database.
 *
 * @MigrateSource(
 *   id = "d7_wb2cm_transitions",
 *   core = {7},
 *   source_module = "workbench_moderation",
 *   destination_module = "content_moderation"
 * )
 */
class Wb2CmTransitions extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('workbench_moderation_transitions', 'wbmt');
    $query->fields('wbmt');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'id' => $this->t('ID'),
      'name' => $this->t('Name'),
      'from_name' => $this->t('From Name'),
      'to_name' => $this->t('To Name'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['name']['type'] = 'string';
    $ids['name']['alias'] = 'wbmt';
    return $ids;
  }

}
