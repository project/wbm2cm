<?php

namespace Drupal\wbm2cm\Plugin\migrate\source\d7;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 webform source from database.
 *
 * @MigrateSource(
 *   id = "d7_wb2cm_states",
 *   core = {7},
 *   source_module = "workbench_moderation",
 *   destination_module = "content_moderation"
 * )
 */
class Wb2CmState extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('workbench_moderation_states', 'wbms');
    $query->fields('wbms');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'name' => $this->t('Name'),
      'label' => $this->t('Label'),
      'description' => $this->t('Description'),
      'weight' => $this->t('weight'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['name']['type'] = 'string';
    $ids['name']['alias'] = 'wbms';
    return $ids;
  }

}
