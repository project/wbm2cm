<?php

namespace Drupal\wbm2cm\Plugin\migrate\destination;

use Drupal\migrate\EntityFieldDefinitionTrait;
use Drupal\migrate\Plugin\migrate\destination\EntityContentComplete;
use Drupal\migrate\Row;

/**
 * Provides a destination for migrating the entire entity revision table.
 *
 * @MigrateDestination(
 *   id = "entity_complete:node"
 * )
 */
class NodeEntityComplete extends EntityContentComplete {

  use EntityFieldDefinitionTrait;

  /**
   * Gets the entity.
   *
   * @param \Drupal\migrate\Row $row
   *   The row object.
   * @param array $old_destination_id_values
   *   The old destination IDs.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  protected function getEntity(Row $row, array $old_destination_id_values) {
    $entity = parent::getEntity($row, $old_destination_id_values);
    if ($entity->hasField('moderation_state')) {
      $state = $row->getSourceProperty('wb_state');
      $published = $row->getSourceProperty('wb_published');
      $is_current = $row->getSourceProperty('wb_is_current');
      $entity->isDefaultRevision(FALSE);
      if ($entity->hasField('moderation_state')) {
        if (!empty($state)) {
          $entity->moderation_state->value = $state;
        }
        else {
          $entity->moderation_state->value = 'draft';
        }
        if ($state == 'published' && !$published) {
          $entity->moderation_state->value = 'draft';
        }
        if ($is_current) {
          if ($published) {
            if ($entity->hasField('moderation_state')) {
              $entity->moderation_state->value = 'published';
            }
          }
          $entity->isDefaultRevision(TRUE);
        }
      }
      else {
        $entity->isDefaultRevision(TRUE);
      }
    }
    return $entity;
  }

}
