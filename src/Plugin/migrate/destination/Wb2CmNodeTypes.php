<?php

namespace Drupal\wbm2cm\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\Config;
use Drupal\migrate\Row;
use Drupal\node\Entity\NodeType;

/**
 * Drupal 8 views destination.
 *
 * @MigrateDestination(
 *   id = "wb2cm_node_types"
 * )
 */
class Wb2CmNodeTypes extends Config {

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    if ($this->isTranslationDestination()) {
      $this->config = $this->language_manager->getLanguageConfigOverride($row->getDestinationProperty('langcode'), $this->config->getName());
    }
    $name = $row->getDestinationProperty('name');
    $value = $row->getDestinationProperty('value');
    $typeSettings = $this->config->get('type_settings');
    $name = str_replace('node_options_', '', $name);
    $nodeType = NodeType::load($name);
    if ($nodeType instanceof NodeType) {
      $typeSettings['entity_types']['node'][] = $name;
    }
    $this->config->set('type_settings', $typeSettings);
    $this->config->save();
    $ids[] = $this->config->getName();
    if ($this->isTranslationDestination()) {
      $ids[] = $row->getDestinationProperty('langcode');
    }
    return $ids;
  }

}
