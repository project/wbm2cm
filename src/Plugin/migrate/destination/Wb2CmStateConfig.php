<?php

namespace Drupal\wbm2cm\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\Config;
use Drupal\migrate\Row;

/**
 * Drupal 8 views destination.
 *
 * @MigrateDestination(
 *   id = "wb2cm_state_config"
 * )
 */
class Wb2CmStateConfig extends Config {

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    if ($this->isTranslationDestination()) {
      $this->config = $this->language_manager->getLanguageConfigOverride($row->getDestinationProperty('langcode'), $this->config->getName());
    }
    $name = $row->getDestinationProperty('name');
    $label = $row->getDestinationProperty('label');
    $weight = $row->getDestinationProperty('weight');
    $destKey = 'type_settings.states.' . $name;
    $value = [
      'label' => $label,
      'weight' => $weight,
      'published' => ($name == 'published'),
      'default_revision' => FALSE,
    ];
    $this->config->set($destKey, $value);
    $this->config->save();
    $ids[] = $this->config->getName();
    if ($this->isTranslationDestination()) {
      $ids[] = $row->getDestinationProperty('langcode');
    }
    return $ids;
  }

}
