<?php

namespace Drupal\wbm2cm\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\Config;
use Drupal\migrate\Row;

/**
 * Drupal 8 views destination.
 *
 * @MigrateDestination(
 *   id = "wb2cm_transition_config"
 * )
 */
class Wb2CmTransitionConfig extends Config {

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    if ($this->isTranslationDestination()) {
      $this->config = $this->language_manager->getLanguageConfigOverride($row->getDestinationProperty('langcode'), $this->config->getName());
    }
    $id = $row->getDestinationProperty('id');
    $name = $row->getDestinationProperty('name');
    $from_name = $row->getDestinationProperty('from_name');
    $to_name = $row->getDestinationProperty('to_name');
    $destKey = 'type_settings.transitions.' . $id;
    $value = [
      'label' => $name,
      'from' => [$from_name],
      'to' => $to_name,
      'weight' => $id,
    ];
    $this->config->set($destKey, $value);
    $this->config->save();
    $ids[] = $this->config->getName();
    if ($this->isTranslationDestination()) {
      $ids[] = $row->getDestinationProperty('langcode');
    }
    return $ids;
  }

}
