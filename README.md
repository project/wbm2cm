Workbench to content moderation migration
------------------------------------------

### About this Module

This module provides migration routines from Drupal 7 workbench moderation states to Drupal Core content moderation states.

### Installing the Webform Migrate Module
- Install the module wbm2cm
- Andthen continue the /upgrade process


### Configure:

- Make sure your user workbench permissions in D7 site.
- Needs manual migration for workbench related user permissions.
